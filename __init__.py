# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import bom
from . import product


def register():
    Pool.register(
        bom.BOMTree,
        bom.OpenBOMTreeTree,
        product.Template,
        bom.OpenBOMTreeStart,
        bom.OpenBOMTreeProducts,
        bom.OpenBOMTreePurpose,
        module='electrans_production_planning', type_='model')
    Pool.register(
        bom.ProductionPlanning,
        module='electrans_production_planning', type_='wizard')
