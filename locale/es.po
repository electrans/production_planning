#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:production.bom.tree,request:"
msgid "Request"
msgstr "Propuesta Solicitud"

msgctxt "field:production.bom.tree,supplier:"
msgid "Supplier"
msgstr "Proveedor"

msgctxt "field:production.bom.tree,icon:"
msgid "Icon"
msgstr "Icono"

msgctxt "field:production.bom.tree,supplier_warehouse:"
msgid "Supplier Warehouse"
msgstr "Almacén Necesidad"

msgctxt "field:production.bom.tree,supplier_stock:"
msgid "Supplier Stock"
msgstr "Stock en A.N."

msgctxt "field:production.bom.tree,supplier_output_stock:"
msgid "Supplier Outputs"
msgstr "Salidas en A.N."

msgctxt "field:production.bom.tree,supplier_input_stock:"
msgid "Supplier Inputs"
msgstr "Entradas en A.N."

msgctxt "field:production.bom.tree,product_type:"
msgid "Product Type"
msgstr "Tipo de solicitud"

msgctxt "field:production.bom.tree,draft_outputs:"
msgid "Draft Output"
msgstr "Salidas en borrador"

msgctxt "field:production.bom.tree,created_requests:"
msgid "Created Requests"
msgstr "Solicitudes creadas"

msgctxt "field:production.bom.tree,request_information:"
msgid "Request information"
msgstr "Información de la propuesta"

msgctxt "field:production.bom.tree.open.products,product:"
msgid "Product"
msgstr "Producto"

msgctxt "field:production.bom.tree.open.products,quantity:"
msgid "Quantity"
msgstr "Cantidad"

msgctxt "field:production.bom.tree.open.products,bom:"
msgid "Bom"
msgstr "Ldm"

msgctxt "field:production.bom.tree.open.products,uom:"
msgid "Uom"
msgstr "Udm"

msgctxt "field:production.bom.tree,total_quantity:"
msgid "Total Quantity"
msgstr "Necesidad Total"

msgctxt "field:production.bom.tree,manual_request:"
msgid "Manual Request"
msgstr "Propuesta Manual"

msgctxt "selection:production.bom.tree,product_type:"
msgid "Producible"
msgstr "Producir"

msgctxt "selection:production.bom.tree,product_type:"
msgid "Purchasable"
msgstr "Comprar"

msgctxt "wizard_button:production.planning,tree,start:"
msgid "Change"
msgstr "Cambiar"

msgctxt "wizard_button:production.planning,tree,reload:"
msgid "Reload"
msgstr "Refrescar"

msgctxt "wizard_button:production.planning,tree,create_requests:"
msgid "Create Requests"
msgstr "Crear Solicitudes"

msgctxt "wizard_button:production.planning,tree,end:"
msgid "Close"
msgstr "Salir"

msgctxt "field:production.bom.tree.open.purpose,purpose:"
msgid "Purpose"
msgstr "Destino"

msgctxt "model:ir.action,name:wizard_recalculation_needs_open_product"
msgid "Recalculation needs"
msgstr "Recálculo de necesidades"

msgctxt "model:ir.action,name:wizard_recalculation_needs_open_sale"
msgid "Recalculation needs"
msgstr "Recálculo de necesidades"

msgctxt "model:ir.action,name:wizard_recalculation_needs_open_template"
msgid "Recalculation needs"
msgstr "Recálculo de necesidades"

msgctxt "wizard_button:production.planning,tree,ask_purpose:"
msgid "Create Requests"
msgstr "Crear solicitud"

msgctxt "field:production.bom.tree.open.start,no_stock_count:"
msgid "No Stock"
msgstr "No tener en cuenta el stock"