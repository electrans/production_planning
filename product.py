# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Or, Not

__all__ = ['Template']
suppliers_domain = Not(Or(Eval('purchasable', False), Eval('producible', False)))


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls.product_suppliers.states['invisible'] = suppliers_domain

    @classmethod
    def view_attributes(cls):
        return super(Template, cls).view_attributes() + [
            ('//page[@id="suppliers"]', 'states', {
                    'invisible': suppliers_domain
            })]
