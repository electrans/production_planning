# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from trytond.pyson import Eval


def append_values(bom_tree, bom_requests, parent_request=None, stock={'current_stock': {}, 'inputs': {}, 'outputs': {}},
                  first_row=False, total_quantity={'repeated': []}):
    pool = Pool()
    Product = pool.get('product.product')
    for bom in bom_tree:
        get_created_requests(bom)
        if first_row:
            parent_request = Transaction().context.get('quantity', 1.0)

        # set input and output stock
        bom['current_stock'] = stock['current_stock'].get(bom['product'], 0.0)
        bom['input_stock'] = stock['inputs'].get(bom['product'], 0.0)
        bom['output_stock'] = stock['outputs'].get(bom['product'], 0.0)

        product = Product(bom['product'])
        if product.producible:
            bom['product_type'] = 'producible'
        elif product.purchasable:
            bom['product_type'] = 'purchasable'

        if 'factor' in bom:
            bom['factor'] = bom['quantity'] / bom['factor']
        else:
            bom['factor'] = bom['quantity']

        get_requests(bom, bom_requests, parent_request, total_quantity)
        if bom.get('icon', '') != 'N.A.':
            get_icon(bom)
        # Prepare a dictionary with the total quantities of each product to get grouped requests
        quantity = (parent_request or 0.0) * bom['factor']
        if bom['product'] in total_quantity:
            if bom['product'] not in total_quantity['repeated']:
                total_quantity['repeated'].append(bom['product'])
            total_quantity[bom['product']].append(quantity)
        else:
            total_quantity[bom['product']] = [quantity]
        if bom['childs']:
            total_quantity.update(append_values(bom['childs'], bom_requests, bom['request'], stock,
                                                total_quantity=total_quantity))
        if 'need_quantity' in bom:
            del bom['need_quantity']

    return total_quantity


def append_values_no_stock(bom_tree, bom_requests, parent_request=None, first_row=False,
                           total_quantity={'repeated': []}):
    pool = Pool()
    Product = pool.get('product.product')
    for bom in bom_tree:
        get_created_requests(bom)
        if first_row:
            parent_request = Transaction().context.get('quantity', 1.0)

        product = Product(bom['product'])
        if product.producible:
            bom['product_type'] = 'producible'
            if 'factor' in bom:
                bom['factor'] = bom['quantity'] / bom['factor']
            else:
                bom['factor'] = bom['quantity']
            # Fill bom_requests variable
            get_requests_no_stock(bom, bom_requests, parent_request, total_quantity)
            # Prepare a dictionary with the total quantities of each product to get grouped requests
            if bom['product'] in total_quantity:
                if bom['product'] not in total_quantity['repeated']:
                    total_quantity['repeated'].append(bom['product'])
                total_quantity[bom['product']].append(bom['quantity'])
            else:
                total_quantity[bom['product']] = [bom['quantity']]
            # Recursive call with the childs of the current product bom
            if bom['childs']:
                total_quantity.update(append_values_no_stock(bom['childs'], bom_requests, bom['request'],
                                                             total_quantity=total_quantity))
            if 'need_quantity' in bom:
                del bom['need_quantity']

    return total_quantity


def get_created_requests(bom):
    """Get the quantity of the created purchase.requests and the purchase.lines without created moves yet"""
    pool = Pool()
    Request = pool.get('purchase.request')
    PurchaseLine = pool.get('purchase.line')
    Move = pool.get('stock.move')

    requests = Request.search([
        ('product', '=', bom['product']),
        ('state', '=', 'pending'),
    ])
    lines = PurchaseLine.search([
        ('product', '=', bom['product']),
        ('purchase.state', 'not in', ['cancelled', 'processing', 'done']),
    ])
    moves = Move.search([
        ('product', '=', bom['product']),
        ('state', '=', 'draft'),
        ('production_output', '!=', None),
        ('production_output.state', '=', 'request'),
    ])
    bom['created_requests'] = (sum(r.quantity for r in requests)
                               + sum(l.quantity for l in lines) + sum(m.quantity for m in moves) or None)


def get_requests(bom, bom_requests, parent_request, total_quantity):
    """
    Define the quantity of the needed purchase or the production and the quantity that have to be moved to
    the location where the need is if needed and there are stock in the warehouse (the one selected on the wizard)
    """
    pool = Pool()
    Product = pool.get('product.product')
    Uom = pool.get('product.uom')
    Bom = pool.get('production.bom')
    manual_request = Transaction().context.get('requests', {}).get(bom['product'])
    if parent_request or manual_request:
        available_stock = bom['current_stock'] - bom['output_stock'] + bom['input_stock']
        if available_stock < 0:
            available_stock = 0
        if manual_request:
            bom['need_quantity'] = bom['request'] = need_quantity = manual_request
            if parent_request:
                original_quantity = parent_request * bom['factor'] + \
                                    (sum(total_quantity[bom['product']]) if bom['product'] in total_quantity else 0.0)
                bom['original_request'] = original_quantity - available_stock \
                    if original_quantity > available_stock else 0
            else:
                bom['original_request'] = 0
        else:
            bom['need_quantity'] = need_quantity = parent_request * bom['factor'] + \
                                                   (sum(total_quantity[bom['product']]) if bom[
                                                                                               'product'] in total_quantity else 0.0)
            bom['request'] = need_quantity - available_stock

    if 'request' not in bom or bom['request'] <= 0:
        bom['request'] = None
    else:
        for request in bom_requests:
            if request['product'] == bom['product']:
                request['request'] = bom['request']
                request['current_stock'] = bom['current_stock']
                request['input_stock'] = bom['input_stock']
                request['output_stock'] = bom['output_stock']
                request['created_requests'] = bom['created_requests']
                request['total_quantity'] = need_quantity
                return
        product_type = bom['product_type'] if 'product_type' in bom else None
        request = {'product': bom['product'], 'product.rec_name': Product(bom['product']).rec_name,
                   'request': bom['request'], 'product_type': product_type,
                   'current_stock': bom['current_stock'], 'input_stock': bom['input_stock'],
                   'output_stock': bom['output_stock'], 'created_requests': bom['created_requests'],
                   'total_quantity': need_quantity, 'uom': bom['uom'], 'uom.rec_name': Uom(bom['uom']).rec_name,
                   'bom': bom['bom'], 'bom.rec_name': Bom(bom['bom']).rec_name if bom['bom'] else None,
                   'original_request': bom['original_request'] if 'original_request' in bom else None,
                   'prevent_productions_without_substitute': bom['prevent_productions_without_substitute']
                   if 'prevent_productions_without_substitute' in bom else None}
        if 'original_request' in bom:
            request['request'] = bom['original_request']
            request['manual_request'] = bom['request']
        if manual_request:
            # when the 'on_change_manual_request' is executed, it's used to do not show the warning to the user
            request['request_information'] = 'remove reload warning'
        bom_requests.append(request)


def get_requests_no_stock(bom, bom_requests, parent_request, total_quantity):
    """
      Get request ignoring stock
    """
    pool = Pool()
    Product = pool.get('product.product')
    Uom = pool.get('product.uom')
    Bom = pool.get('production.bom')
    manual_request = Transaction().context.get('requests', {}).get(bom['product'])
    if parent_request or manual_request:
        if manual_request:
            bom['need_quantity'] = bom['request'] = need_quantity = manual_request
            if parent_request:
                original_quantity = parent_request * bom['factor'] + \
                                    (sum(total_quantity[bom['product']]) if bom['product'] in total_quantity else 0.0)
                bom['original_request'] = original_quantity
            else:
                bom['original_request'] = 0
        else:
            bom['need_quantity'] = need_quantity = parent_request * bom['factor'] + \
                                    (sum(total_quantity[bom['product']]) if bom['product'] in total_quantity else 0.0)
            bom['request'] = need_quantity

    if 'request' not in bom or bom['request'] <= 0:
        bom['request'] = None
    else:
        for request in bom_requests:
            # If production request already exists, accumulate the quantity needed
            if request['product'] == bom['product']:
                request['request'] = bom['request']
                request['created_requests'] = bom['created_requests']
                request['total_quantity'] = need_quantity
                return
        product_type = bom['product_type'] if 'product_type' in bom else None
        # Production request that will be generated
        request = {'product': bom['product'], 'product.rec_name': Product(bom['product']).rec_name,
                   'request': bom['request'], 'product_type': product_type, 'created_requests': bom['created_requests'],
                   'total_quantity': need_quantity, 'uom': bom['uom'], 'uom.rec_name': Uom(bom['uom']).rec_name,
                   'bom': bom['bom'], 'bom.rec_name': Bom(bom['bom']).rec_name if bom['bom'] else None,
                   'original_request': bom['original_request'] if 'original_request' in bom else None}

        if 'prevent_productions_without_substitute' in bom:
            request['prevent_productions_without_substitute'] = bom['prevent_productions_without_substitute']
        if 'original_request' in bom:
            request['request'] = bom['original_request']
            request['manual_request'] = bom['request']
        if manual_request:
            # when the 'on_change_manual_request' is executed, it's used to do not show the warning to the user
            request['request_information'] = 'remove reload warning'
        # Updates bom_requests variable
        bom_requests.append(request)


def get_icon(bom, total_quantity=None):
    """Set an icon to each bill of material child depending on the stock"""

    def modify_childs_icon(childs):
        """Set the bom child icons to 'N.A' on each child of a parent with available stock to supply the need."""
        for child in childs:
            child['icon'] = 'N.A.'
            if child['childs']:
                modify_childs_icon(child['childs'])

    quantity = total_quantity or bom.get('need_quantity', 0.0)
    created_requests = bom['created_requests'] or 0.0
    if bom['current_stock'] - bom['output_stock'] >= quantity:
        bom['icon'] = 'check'
    elif bom['current_stock'] + bom['input_stock'] + created_requests - \
            bom['output_stock'] >= quantity:
        bom['icon'] = 'check2'
    else:
        bom['icon'] = 'error'
        return bom
    if bom['childs']:
        modify_childs_icon(bom['childs'])
    return bom


def get_input_output_product(products, location_ids, name):
    """Get all input/output products"""
    transaction = Transaction()
    context = transaction.context
    date_end = context.get('stock_date_end')

    production_state = ""
    planned_date = ""
    products_clause = str(tuple(products)) if len(products) > 1 else "(" + str(products[0]) + ") "
    if name == 'input_stock':
        state_clause = "m.state ='draft'"
        locations_clause = "to_location in " + str(tuple(location_ids)) \
                           + "and from_location not in " + str(tuple(location_ids))
        production_state = " and (p.state != 'request' or p.state is null)"
        planned_date = " and  m.planned_date <= '" + str(date_end) + "' "
    else:
        state_clause = "(m.state ='draft' or m.state = 'assigned')"
        locations_clause = "from_location in " + str(tuple(location_ids)) \
                           + " and to_location not in " + str(tuple(location_ids))

    result = {}
    product2lines = {}
    for product in products:
        result[product] = 0
        product2lines.setdefault(product, []).append(product)

    cursor = transaction.connection.cursor()

    query = "select m.product, sum(m.internal_quantity) from stock_move m" \
            + " left join production p on p.id = m.production_output" \
            + " where m.product in " + products_clause \
            + " and " + locations_clause \
            + " and " + state_clause \
            + " and (p.state != 'request' or p.state is null)" \
            + planned_date \
            + production_state \
            + " group by m.product"
    cursor.execute(query)
    for product_id, quantity in cursor.fetchall():
        for line in product2lines[product_id]:
            result[line] = quantity
    return result


class OpenBOMTreeProducts(ModelSQL, ModelView, metaclass=PoolMeta):
    """OpenBOMTreeStart"""
    __name__ = 'production.bom.tree.open.products'

    product = fields.Many2One('product.product', 'Product', required=1)
    quantity = fields.Float('Quantity', required=1)
    bom = fields.Many2One('production.bom', 'Bom',
                          domain=[('output_products', '=', Eval('product', 0))],
                          search_context={'active_version': True},
                          depends=['product'])
    uom = fields.Many2One('product.uom', 'Uom', required=1)

    @staticmethod
    def default_quantity():
        return 1.0

    @fields.depends('product', 'uom', 'bom')
    def on_change_product(self):
        self.bom = self.product.boms[0].active_version if self.product and self.product.boms else None
        self.uom = self.product.default_uom if self.product else None


class OpenBOMTreeStart(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open.start'

    products = fields.One2Many('production.bom.tree.open.products', None, 'Products', required=1)

    @staticmethod
    def default_products():
        pool = Pool()
        Product = pool.get('product.product')
        Sale = pool.get('sale.sale')
        Template = pool.get('product.template')
        defaults = []
        transaction = Transaction()
        products = []
        if transaction.context.get('active_model', '') == 'sale.sale':
            for id in transaction.context.get('active_ids', []):
                sale = Sale(id)
                for line in sale.lines:
                    products.append(line.product)
        if transaction.context.get('active_model', '') == 'product.template':
            for id in transaction.context.get('active_ids', []):
                template = Template(id)
                if template.products:
                    products.append(template.products[0])
        else:
            for id in transaction.context.get('active_ids', []):
                products.append(Product(id))
        for product in products:
            defaults.append({
                'product': product.id, 'quantity': 1,
                'uom': product.default_uom.id if product.default_uom else None,
                'bom': product.boms[0].active_version.id if
                product.boms and product.boms[0].active_version else None
            })
        return defaults


class OpenBOMTreePurpose(ModelView):
    'Open BOM Tree Purpose'
    __metaclass__ = PoolMeta
    __name__ = 'production.bom.tree.open.purpose'

    purpose = fields.Char('Purpose')


class ProductionPlanning(Wizard, metaclass=PoolMeta):
    __name__ = 'production.planning'

    start = StateView('production.bom.tree.open.start',
                      'electrans_production_planning.bom_planning_tree_open_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('OK', 'tree', 'tryton-ok', True),
                      ])
    tree = StateView('production.bom.tree.open.tree',
                     'electrans_production_planning.production_planning_view_form', [
                         Button('Change', 'start', 'tryton-go-previous'),
                         Button('Reload', 'reload', 'tryton-refresh'),
                         Button('Create Requests', 'ask_purpose', 'tryton-go-next'),
                         Button('Close', 'end', 'tryton-close'),
                     ])
    ask_purpose = StateView('production.bom.tree.open.purpose',
                            'electrans_production_planning.production_planning_purpose_view_form2', [
                                Button('Previous', 'tree', 'tryton-go-previous'),
                                Button('OK', 'create_requests', 'tryton-ok', True),
                            ])

    reload = StateTransition()
    create_requests = StateTransition()

    # TODO: It's the same code as default_start on production.bom.tree.open but I don't know if there is aa way to call
    #  it, because the class only extend from Wizard.
    def default_start(self, fields):
        Date = Pool().get('ir.date')
        defaults = {}
        defaults['date'] = Date.today()
        defaults['no_stock_count'] = True
        self.tree.requests = None
        return defaults

    def default_tree(self, fields):
        requests = {}
        if hasattr(self.tree, 'requests'):
            for request in self.tree.requests:
                if request.manual_request:
                    requests[request.product.id] = request.manual_request
        pool = Pool()
        BomTree = pool.get('production.bom.tree.open.tree')
        # TODO: remove locations parameter in context, and do not call super BomTree.tree wich is using it
        with Transaction().set_context(tree_date=self.start.date, stock_date_end=self.start.date, requests=requests,
                                       locations=[4], products=self.start.products, no_stock=True, with_childs=True,
                                       no_stock_count=self.start.no_stock_count):
            return BomTree.tree(self.start.products[0].bom, self.start.products[0].product,
                                self.start.products[0].quantity, self.start.products[0].uom)

    def transition_create_requests(self):
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        Request = pool.get('purchase.request')
        User = pool.get('res.user')
        company = User(Transaction().user).company
        for request in self.tree.requests:
            quantity = request.manual_request or request.request
            # TODO: create a field where the user can specify the warehouse
            warehouse = 4
            if quantity:
                if request.product.producible and not request.product.phantom:
                    with Transaction().set_context(bom=request.bom):
                        prod = Production.compute_request(
                            request.product, Location(warehouse), quantity,
                            self.start.date, company)
                    prod.reference = self.ask_purpose.purpose
                    prod.planned_start_date = self.start.date
                    prod.explode_bom()
                    prod.save()
                elif request.product.purchasable and not request.product.phantom:
                    req = Request(
                        product=request.product, quantity=quantity,
                        uom=request.uom, computed_uom=request.uom,
                        company=company, warehouse=warehouse)
                    req.purpose = self.ask_purpose.purpose
                    req.save()
        return 'end'

    def transition_reload(self):
        self.start.information_message = None
        return 'tree'


class BOMTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree'

    icon = fields.Char('Icon', readonly=True)
    request = fields.Float('Request', readonly=True)
    manual_request = fields.Float('Manual Request')
    supplier = fields.Many2One('party.party', 'Supplier', readonly=True)
    # warehouse where the exists the need
    supplier_warehouse = fields.Many2One('stock.location', 'Supplier warehouse', readonly=True)
    supplier_stock = fields.Float('Warehouse stock', readonly=True)
    supplier_input_stock = fields.Float('Supplier Inputs', readonly=True)
    supplier_output_stock = fields.Float('Supplier Outputs', readonly=True)
    # Units of a child(product) needed to produce each parent unit
    # (It is the number by which the child quantity are multiplied)
    factor = fields.Float("Factor", readonly=True)
    product_type = fields.Selection([
        ('', ''),
        ('purchasable', 'Purchasable'),
        ('producible', 'Producible')
    ], 'Product Type', readonly=True)
    created_requests = fields.Float('Created Requests', readonly=True)
    request_information = fields.Char('Request information', readonly=True)
    total_quantity = fields.Float('Total Quantity', readonly=True)
    # If reload is True, it's needed to reload the view to recalculate the requests
    reload = fields.Boolean('Need to Reload', readonly=True)

    @classmethod
    def __setup__(cls):
        super(BOMTree, cls).__setup__()
        cls.quantity.digits = None

    @fields.depends('manual_request', 'request', 'product_type', 'request_information')
    def on_change_manual_request(self):
        # TODO: create translations
        if self.request_information == 'remove reload warning':
            self.request_information = ''
        elif not self.manual_request:
            self.request_information = ''
            self.reload = False
        elif self.product_type == 'producible':
            self.reload = True
            self.request_information = "Es necesario actualizar el arbol."
        elif self.manual_request < self.request:  # and getattr(self, 'manual_request', None):
            self.request_information = 'La propuesta manual es inferior a la generada por el sistema.'
        else:
            self.request_information = None


class OpenBOMTreeTree(metaclass=PoolMeta):
    __name__ = 'production.bom.tree.open.tree'

    requests = fields.One2Many('production.bom.tree', None, 'BOM Tree')
    information_message = fields.Char('Message Information', readonly=True)

    @classmethod
    def tree(cls, bom, product, quantity, uom):
        def get_total_quantity(bom_tree, total_quantity, requests):
            "Get the total quantity of each product in the whole tree and update the icon"
            for bom in bom_tree:
                if bom['product'] in requests:
                    bom['request'] = requests[bom['product']]
                if bom['product'] in total_quantity['repeated']:
                    bom['request'] = sum(total_quantity[bom['product']])
                    if bom.get('icon', None) and bom.get('icon', '') != 'N.A.':
                        get_icon(bom, bom.get('total_quantity', bom.get('quantity')))
                if bom['childs']:
                    get_total_quantity(bom['childs'], total_quantity, requests)

        def get_all_bom_products(bom_tree):
            products = []
            for bom in bom_tree:
                products.append(Product(bom['product']))
                if bom['childs']:
                    products += get_all_bom_products(bom['childs'])
            return products

        transaction = Transaction()
        pool = Pool()
        Location = pool.get('stock.location')
        Product = pool.get('product.product')
        bom_tree = {'bom_tree': [], 'requests': []}
        with transaction.set_context(get_substitutes=True):
            if transaction.context.get('products'):
                if transaction.context.get('no_stock_count'):
                    # bom_tree will be filled with the first products list element and all his childs in the childs key
                    bom_tree['bom_tree'] = super(OpenBOMTreeTree, cls).tree(bom, product, quantity, uom)['bom_tree']
                    # Inside this method, calls get_requests_no_stock that will fill
                    # bom_tree['requests'] with productions that will be created
                    total_quantity = append_values_no_stock(bom_tree['bom_tree'], bom_tree['requests'],
                                                            first_row=True, total_quantity={'repeated': []})
                else:
                    # TODO: get the warehouse from a field of the wizard
                    warehouses = [location.id for location in Location.search([('id', '=', 4)])]
                    with transaction.set_context(locations=warehouses):
                        for line in transaction.context.get('products'):
                            if line.product.producible and line.bom:
                                bom_tree['bom_tree'].append(
                                    super(OpenBOMTreeTree, cls).tree(line.bom, line.product, line.quantity, line.uom)[
                                        'bom_tree'][0]
                                )
                            else:
                                bom_tree['bom_tree'].append(
                                    {'product': line.product.id, 'product.rec_name': line.product.rec_name,
                                     'uom': line.uom.id, 'uom.rec_name': line.uom.rec_name,
                                     'quantity': line.quantity, 'childs': [], 'current_stock': None})
                        # get the current stock, the inputs and the outputs of all the products in the bom tree
                        all_bom_products = get_all_bom_products(bom_tree['bom_tree'])
                        product_ids = list(map(int, all_bom_products))
                        stock = {}

                        storage_locations = []
                        for wh in Location.search([('id', 'in', warehouses)]):
                            if wh.storage_location:
                                storage_locations.append(wh.storage_location.id)

                        all_wh_location_ids = [l.id for l in Location.search([
                            ('parent', 'child_of', warehouses),
                        ], order=[])]
                        with transaction.set_context(with_childs=True):
                            stock['current_stock'] = Product._get_quantity(all_bom_products, 'quantity',
                                                                           storage_locations,
                                                                           grouping_filter=(product_ids,))
                        stock['inputs'] = get_input_output_product(product_ids, all_wh_location_ids, 'input_stock')
                        stock['outputs'] = get_input_output_product(product_ids, all_wh_location_ids, 'output_stock')
                        total_quantity = append_values(bom_tree['bom_tree'], bom_tree['requests'],
                                                       transaction.context.get('locations')[0],
                                                       first_row=True, total_quantity={'repeated': []}, stock=stock)
                if total_quantity['repeated']:
                    requests = {}
                    for request in bom_tree['requests']:
                        if request['product'] in total_quantity['repeated']:
                            request['request'] = request['total_quantity'] = request.get('manual_request', None) or sum(total_quantity[request['product']])
                    get_total_quantity(bom_tree['bom_tree'], total_quantity, requests)
            else:
                bom_tree = super(OpenBOMTreeTree, cls).tree(bom, product, quantity, uom)
        return bom_tree

    @fields.depends('requests', 'information_message')
    def on_change_requests(self):
        # TODO: create translation
        for request in self.requests:
            # When the wizard is executed from the web client, requests do no has the reload attr
            if getattr(request, 'reload', False):
                self.information_message = 'Es necesario actualizar la vista.'
                return
        self.information_message = ''
