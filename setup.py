import io
import os
import re
from configparser import ConfigParser
from setuptools import setup, find_packages

name = 'electrans_production_planning'

ELECTRANS_MODULES = [
    'electrans_production_bom_versions',
]
TRYTONSPAIN_MODULES = [
]
NAN_TIC_MODULES = [
]
NANTIC_MODULES = [
    'production_bom_stock_form',
    'stock_calculation',
]
ZIKZAKMEDIA_MODULES = [
]
TRYTONZZ_MODULES = [
]
FORZED_DEPENDENCIES = [
    'stock_calculation',
]


def read(fname, slice=None):
    content = io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()
    if slice:
        content = '\n'.join(content.splitlines()[slice])
    return content


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require


config = ConfigParser()
config.read_file(open(os.path.join(os.path.dirname(__file__), 'tryton.cfg')))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)

local_version = []
if os.environ.get('CI_JOB_ID'):
    local_version.append(os.environ['CI_JOB_ID'])
else:
    for build in ['CI_BUILD_NUMBER', 'CI_JOB_NUMBER']:
        if os.environ.get(build):
            local_version.append(os.environ[build])
        else:
            local_version = []
            break
if local_version:
    version += '+' + '.'.join(local_version)

branch = '%s.%s' % (major_version, minor_version)
requires = []
depends = info.get('depends', [])
# Add forced dependencies
if FORZED_DEPENDENCIES:
    for dependency in FORZED_DEPENDENCIES:
        if dependency not in depends:
            depends.append(dependency)
for dep in depends:
    if dep == 'account_invoice_discount':
        requires.append(
        'nan-tic-account_invoice_discount @ git+https://github.com/NaN-tic/trytond-account_invoice_discount.git@6.0#egg=nan-tic-account_invoice_discount-6.0')
    elif dep in NANTIC_MODULES:
        requires.append('nantic-%(dep)s @ https://github.com/NaN-tic/'
            'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
            % {'branch': branch, 'dep': dep})
    elif dep in NAN_TIC_MODULES:
        requires.append('nan-tic-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONSPAIN_MODULES:
        requires.append('trytonspain-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ZIKZAKMEDIA_MODULES:
        requires.append('zikzakmedia-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONZZ_MODULES:
        requires.append('trytonzz-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ELECTRANS_MODULES:
        if dep == 'project_expenses':
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            'trytond-%(dep)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep})
        elif dep == 'electrans_tools':
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            '%(dep)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep})
        else:
            repo_name = dep.replace('electrans_', '')
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            '%(repo_name)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep, 'repo_name': repo_name})
    elif not re.match(r'(ir|res)(\W|$)', dep):
        requires.append(get_require_version('trytond_%s' % dep))

requires.append(get_require_version('trytond'))

tests_require = [
    get_require_version('proteus'),
]

setup(name=name,
      version=version,
      description='Tryton module for multiple customizations',
      long_description=read('README'),
      author='Electrans',
      author_email='jluna@electrans.com',
      url='http://www.electrans.com/',
      download_url="https://bitbucket.org/electrans/%s" % name,
      package_dir={'trytond.modules.electrans_production_planning': '.'},
      packages=(
              ['trytond.modules.%s' % name]
              + ['trytond.modules.electrans_production_planning.%s' % p for p in find_packages()]
      ),
      package_data={
          'trytond.modules.electrans_production_planning': (info.get('xml', [])
                                                     + ['tryton.cfg', 'view/*.xml', 'locale/*.po', '*.fodt',
                                                        'icons/*.svg', 'tests/*.rst', '*.html']),
      },
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Plugins',
          'Framework :: Tryton',
          'Intended Audience :: Developers',
          'Intended Audience :: Financial and Insurance Industry',
          'Intended Audience :: Legal Industry',
      ],
      license='GPL-3',
      install_requires=requires,
      zip_safe=False,
      entry_points="""
        [trytond.modules]
        electrans_production_planning = trytond.modules.electrans_production_planning
        """,
      test_suite='tests',
      test_loader='trytond.test_loader:Loader',
      tests_require=tests_require,
      )
